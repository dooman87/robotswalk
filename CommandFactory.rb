require "singleton"
require "logger"

require "Commands"

# This class provide ability to construct 
# commands with arguments from strings.

class CommandFactory
  include Singleton

  def init
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::INFO
    @COMMANDS = {
      "PLACE"  => PlaceCommand,
      "MOVE"   => MoveCommand,
      "LEFT"   => LeftCommand,
      "RIGHT"  => RightCommand,
      "REPORT" => ReportCommand,
      "MAP"    => MapCommand
    }
  end

# Construct command with name [cmd] and args [args]. 
# All input strings will be transform to upper case
  def cmd (cmd, args = nil)
    if cmd.nil?
      return nil
    end

    if @COMMANDS.nil?
      init
    end

    cmd.upcase!
    if not args.nil?
      args.each {|a| a.upcase!}
    end

    commandClass = @COMMANDS[cmd]
    begin
      @logger.debug("Trying to create command [" + commandClass.to_s + "] with args " + args.to_s)
      args = prepareArgs(commandClass, args)
      return commandClass.new(*args)
    rescue => err
      @logger.error("CommandFactory: Error create command [" + cmd + "]: [" + err.message + "]")
    end
    return nil
  end

  def prepareArgs(commandClass, args)
    resultArgs = []
    if commandClass == PlaceCommand
      resultArgs = preparePlaceArgs(args)
    end
    return resultArgs
  end

  def preparePlaceArgs(args)
    if args.nil? or args.length != 3 or
      not isNumeric(args[0]) or not isNumeric(args[1])
      return nil
    end
    result = []
    result << Integer(args[0]) rescue nil
    result << Integer(args[1]) rescue nil
    result << args[2].to_sym
  end
  
  def isNumeric(str)
    return true if str =~ /^\d+$/
  end
end