require "Robot"
require "Parser"
require "CommandFactory"

def initRobot
  Robot.instance.placed = false
  Robot.instance.x = 0
  Robot.instance.y = 0
  Robot.instance.face = :NORTH
end

initRobot

if ARGF.file == STDIN
  print "Welcome!\nPlease, start with <PLACE x y face> command to start your walking\n"
end

while not ARGF.eof?
  line = ARGF.readline
  words = parseLine(line)
  if not words.nil? and words.length > 0
    cmd = CommandFactory.instance.cmd(words[0], words[1..-1])
    if not cmd.nil?
      cmd.execute
    end
  end
end
