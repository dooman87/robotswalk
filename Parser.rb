# Break line in array which contains words
# Remove all empty words and trailing/leading spaces
# Returns empty array if [str] is nil
def parseLine(str)
  if str.nil?
    return []
  end

  words = str.split(/\W+/)
  words.delete("")
  words
end