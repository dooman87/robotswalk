require "AppConstants"

class PlaceCommand 

  def initialize(x, y, face)
    @x = x
    @y = y
    @face = face
  end

  def execute
    if @x.is_a?(Fixnum) and VALID_ROBOT_POSITION_X.include?(@x) and 
        @y.is_a?(Fixnum) and VALID_ROBOT_POSITION_Y.include?(@y) and
        DIRECTIONS.include?(@face)
      Robot.instance.x = @x
      Robot.instance.y = @y
      Robot.instance.face = @face
      Robot.instance.placed = true
    end
  end

end

class NotFirstCommand
  def initialize
    if not self.respond_to?("executeImpl")
      raise RuntimeError.new("No method executeImpl found")
    end
  end

  def execute
    if Robot.instance.placed == true
      executeImpl
    end
  end
end

class MoveCommand < NotFirstCommand

  def executeImpl
    x = Robot.instance.x
    y = Robot.instance.y

    case Robot.instance.face
    when :NORTH 
      y += 1
    when :SOUTH 
      y -= 1
    when :EAST
      x += 1
    when :WEST
      x -= 1
    end
    if VALID_ROBOT_POSITION_X.include?(x) and VALID_ROBOT_POSITION_Y.include?(y)
      Robot.instance.x = x
      Robot.instance.y = y
    end
  end

end

class RotateCommand < NotFirstCommand
  def rotate(where)
    newIdx = DIRECTIONS.index(Robot.instance.face) + where;
    if newIdx > (DIRECTIONS.length - 1)
      newIdx = 0
    end
    Robot.instance.face = DIRECTIONS[newIdx]
  end
end

class LeftCommand < RotateCommand
  def executeImpl
    rotate(-1)
  end
end

class RightCommand < RotateCommand
  def executeImpl
    rotate(1)
  end
end

class ReportCommand < NotFirstCommand
  def executeImpl
    print Robot.instance.to_s + "\n"
  end
end

class MapCommand < NotFirstCommand
  def executeImpl
    mapY = VALID_ROBOT_POSITION_Y.to_a.reverse
    MAP_HEIGHT.times {|y|
      VALID_ROBOT_POSITION_X.each {|x|
        if mapY[y] == Robot.instance.y and x == Robot.instance.x
          print "X "
        else
          print ". "
        end
      }
      print "\n"
    }
  end
end