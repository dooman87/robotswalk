require "test/AbstractTestCommand"

class TestMoveCommand < AbstractTestCommand
  alias :superSetup :setup
  def setup
    superSetup
    @move = MoveCommand.new
  end

  def test_moveCommandNotPlaced
    @move.execute
    checkInitialRobotPlace
  end

  def test_moveCommand
    place(1, 2, :WEST)

    @move.execute
    checkRobotPlace(true, 0, 2, :WEST)
  end

  def test_moveCommandOutside
    place(4, 2, :EAST)

    @move.execute
    checkRobotPlace(true, 4, 2, :EAST)
  end
end