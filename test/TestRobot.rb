require "minitest/autorun"

require "Robot"

class TestRobot < MiniTest::Unit::TestCase
  def setup
    @robot = Robot.instance
  end

  def test_create
    assert @robot != nil
  end

  def test_robotPosition
    @robot.x = 0;
    @robot.y = 0;
    @robot.face = :NORTH;

    assert_equal 0, @robot.x
    assert_equal 0, @robot.y
    assert_equal :NORTH, @robot.face
  end
end

