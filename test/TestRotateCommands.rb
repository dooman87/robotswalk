require "test/AbstractTestCommand"

class TestRotateCommands < AbstractTestCommand
  alias :superSetup :setup
  def setup
    superSetup
    @left = LeftCommand.new
    @right = RightCommand.new
  end

  def test_leftCommandNotPlaced
    @left.execute
    checkInitialRobotPlace
  end

  def test_leftCommandNorth
    place(1, 2, :NORTH)

    @left.execute
    checkRobotPlace(true, 1, 2, :WEST)
  end

  def test_leftCommandSouth
    place(1, 2, :SOUTH)

    @left.execute
    checkRobotPlace(true, 1, 2, :EAST)
  end

  def test_rightCommandWest
    place(1, 2, :WEST)

    @right.execute
    checkRobotPlace(true, 1, 2, :NORTH)
  end

  def test_rightCommandNorth
    place(1, 2, :NORTH)

    @right.execute
    checkRobotPlace(true, 1, 2, :EAST)
  end
end