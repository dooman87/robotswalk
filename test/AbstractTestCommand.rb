require "minitest/autorun"

require "Robot"
require "Commands"

class AbstractTestCommand < MiniTest::Unit::TestCase
  def setup
    @robot = Robot.instance;
    @robot.placed = false
    @robot.x = 0
    @robot.y = 0
    @robot.face = :NORTH
  end

  def checkInitialRobotPlace
    checkRobotPlace(false, 0, 0, :NORTH)
  end

  def checkRobotPlace(placed, x, y, face)
    assert_equal placed, @robot.placed, "Wrong [PLACED] value"
    assert_equal x, @robot.x, "Wrong [X] position"
    assert_equal y, @robot.y, "Wrong [Y] position"
    assert_equal face, @robot.face, "Wrong [FACE] value"
  end

  def place(x, y, face)
    place = PlaceCommand.new(x, y, face)
    place.execute
  end
end