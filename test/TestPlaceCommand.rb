require "test/AbstractTestCommand"

class TestPlaceCommand < AbstractTestCommand
  def test_placeCommand
    place(1, 2, :WEST)
    checkRobotPlace(true, 1, 2, :WEST)
  end

  def test_wrongPlaceX
    placeWrong(-1, 2, :WEST)
  end

  def test_wrongPlaceY
    placeWrong(1, -2, :WEST)
  end

  def test_wrongPlaceFace
    placeWrong(1, 2, :WRONG_DIRECTION)
  end

  def test_wrongPlaceXFloat
    placeWrong(1.3, 2, :WEST)
  end

  def test_wrongPlaceType
    placeWrong("1", 2, :WEST)
  end

  def placeWrong(x, y, face)
    place(x, y, face)
    checkInitialRobotPlace
  end
end