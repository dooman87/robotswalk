require "CommandFactory"
require "test/AbstractTestCommand"

class TestCommandFactory < AbstractTestCommand
  alias :superSetup :setup

  def setup
    superSetup
    @commandFactory = CommandFactory.instance
  end

  def test_createPlaceCommand
    cmd = @commandFactory.cmd("PLACE", ["1", "2", "south"])
    assert cmd.is_a?(PlaceCommand), "cmd is not a PlaceCommand object"
    cmd.execute
    checkRobotPlace(true, 1, 2, :SOUTH)
  end

  def test_failCreatePlaceCommandNil
    failPlaceCommand
  end

  def test_failCreatePlaceCommandEmpty
    failPlaceCommand([])
  end

  def test_failCreatePlaceCommandArgsCount
    failPlaceCommand(["21"])
  end

  def test_failCreatePlaceCommandArgsType
    failPlaceCommand(["21 baloon", "5", "NORTH"])
  end

  def failPlaceCommand(args = nil)
    assert @commandFactory.cmd("PLACE", args).nil?
  end

  def test_createMoveCommand
    cmd = @commandFactory.cmd("MOVE")
    assert cmd.is_a?(MoveCommand), "cmd is not a MoveCommand object, but is " + cmd.class.to_s
  end

  def test_createCommandLowerCase
    cmd = @commandFactory.cmd("move")
    assert cmd.is_a?(MoveCommand), "cmd is not a MoveCommand object, but is " + cmd.class.to_s
  end

  def test_createCommandNil
    cmd = @commandFactory.cmd(nil)
    assert cmd.nil?
  end
end