require "minitest/autorun"
require "Parser"

class TestParser < MiniTest::Unit::TestCase
  def test_simpleParseLine
    assert_equal ["Hello", "a", "b"], parseLine("Hello a b")
  end

  def test_parseLineSpaces
    assert_equal ["PLACE", "2", "3", "NORTH"], parseLine("   PLACE    2    3  NORTH")
  end

  def test_parseLineNewLine
    assert_equal ["PLACE"], parseLine("PLACE\n")
  end

  def test_parseEmptyLine
    assert_equal [], parseLine("")
  end

  def test_parseNil
    assert_equal [], parseLine(nil)
  end
end