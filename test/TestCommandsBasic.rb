require "test/AbstractTestCommand"

class TestCommandsBasic < AbstractTestCommand
  def test_initialRobotState
    checkInitialRobotPlace
  end


  class WrongCommand < NotFirstCommand
    
  end

  def test_wrongCommand 
    assert_raises(RuntimeError, "No method executeImpl found") {WrongCommand.new}
  end
end