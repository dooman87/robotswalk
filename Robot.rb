require "singleton"

# Base class that represents a walking robot
#
# In this implementation, we can have only one robot

class Robot
  include Singleton

  attr_accessor :x, :y, :face, :placed

  def to_s
    @x.to_s + "," + @y.to_s + "," + @face.to_s
  end
end